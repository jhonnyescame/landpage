var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var htmlmin = require('gulp-htmlmin');

// TASK PADRÃO	
gulp.task('default',['sass','js','htmlmin','watch']);

// TASK SASS
gulp.task('sass', function () {
    return gulp.src('sass/**/*.scss')
    	.pipe(concat('style_min.css'))
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(gulp.dest('css'));
});

//TASK JS MIN
gulp.task('js', function() {
    return gulp.src('_js/**/*.js')
        .pipe(concat('script_min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('js'));
});

// TASK HTML MIN
gulp.task('htmlmin', function() {
    return gulp.src('_html/**/*.html')
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest('.'))
});


// WATCH
gulp.task('watch', function() {
    gulp.watch('sass/**/*.scss',['sass']);
    gulp.watch('_js/**/*.js',['js']);
    gulp.watch('_html/**/*.html',['htmlmin']);
});